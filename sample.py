import argparse

from runner import run
from pipelines import SomeAnalysis

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    # optional args
    parser.add_argument("--datemask", help="""A string in the format '%Y-%m-%d', used to select input log files to include in the jobflow.""")
    parser.add_argument("-emr", help=""" Flag to skip emr job; when set, directly process outstanding job output in memory.""", action='store_true')
    args = parser.parse_args()

    analysis = SomeAnalysis("name of some db collection for writes",
                                                args.datemask, 
                                                "s3://path/to/pig/script",
                                                ["relative/s3/output/path/1", ...])

    if not args.emr:
        run(analysis)
    else:
        run(analysis, emr=False)
