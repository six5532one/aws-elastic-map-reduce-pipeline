"""
Utilities for executing a data analysis pipeline 
(such as the one implemented in runner.py).
This module uses an object oriented approach to represent
analyses, such that each analysis subclasses a base class
(EMRMongoPipeline) to set attributes including
the path to the pig script that executes the analysis,
the dates of the raw logs to run the EMR job on and the
local storage for the processed result of the analysis.

In addition to setting these attributes, each child of 
EMRMongoPipeline specifies a method for processing the
EMR output in memory before storing the final results locally.

"""

import pymongo
import argparse
import os
import re
import time
import datetime
import subprocess
import logging
import string
import boto
import numpy as np

from boto.s3.connection import S3Connection
from boto.emr.instance_group import InstanceGroup
from boto.emr.step import InstallPigStep, PigStep
from bson import json_util
from abc import ABCMeta, abstractmethod
from cStringIO import StringIO
from collections import defaultdict

import settings
from pymongo.errors import ConnectionFailure
from settings import MONGODB_NAME, LASTRUNS_COLLECTION_NAME, GARBAGE_COLLECTION_PERIOD, get_db


log = logging.getLogger()


try:
    db = get_db()
except ConnectionFailure:
    log.error("Could not connect to MongoDB. Try service restart....")
    try:
        mongo_restart = subprocess.Popen(['sudo', 'service', 'mongodb', 'restart'], stderr=subprocess.STDOUT)
    except ConnectionFailure:
        raise


def check_subprocess(subprocess_name, pid, datestring):
    """Log success status of a subprocess handled by the subprocess.Popen class."""
    pid, status = os.waitpid(pid, 0)
    if not status:
        log.info("{subprocess} succeeded for {dates}".format(subprocess=subprocess_name,
                                                             dates=datestring))
        return True
    else:
        log.error("{subprocess} failed for {dates}".format(subprocess=subprocess_name,
                                                           dates=datestring))
        return False


def get_sourcebucket(bucket_name):
    """Return the S3 bucket corresponding to the specified bucket name."""
    s3connection = S3Connection()
    return s3connection.get_bucket(bucket_name)


def get_writebuffer(temp_root):
    """Return a temp file buffer for writing the contents of an S3 key."""
    if not os.path.exists(temp_root):
        os.makedirs(temp_root)
    temp_out = "{rootpath}/{pid}.gz".format(rootpath=temp_root, pid=os.getpid())
    # in POSIX mode, zcat will append '.Z' to all filenames that don't have that suffix
    posix_mode_suffix = ".Z"
    return "{origfilename}{suffix}".format(origfilename=temp_out,
                                           suffix=posix_mode_suffix)


class EMRMongoPipeline(object):

    """Execute pipeline to process raw log data with EMR and write results to MongoDB.

    This abstract base class launches an EMR pig jobflow and reads data
    from the output keys in S3. Each concrete implementation corresponds
    to a pipeline that launches, processes and writes output for a jobflow
    that executes a single pig script.

    """

    __metaclass__ = ABCMeta 
    dateformat = "%Y-%m-%d"
    source_bucket = get_sourcebucket("parselypig")
    processed_logs_path = "output/processed_logs/.all_apikeys"
    base_input_path = "s3://parselypig"
    writebuffer = get_writebuffer("/tmp/pig_output")
    json_out = "network_records.json"


    def __init__(self,
                 analysis_name,
                 datemask, 
                 pigscriptfile,
                 s3_bucket_prefixes):
        """Keyword arguments:
        analysis_name -- name of collection that stores final processed data for this analysis
        datemask -- regex arg to pig script for selecting raw log input files by date
        pigscriptfile -- path to the pig script in S3
        s3_bucket_prefixes -- list of S3 paths to store output of the EMR job

        """
        log.info("Initializing pipeline")
        self.bucket_prefixes = s3_bucket_prefixes
        self.analysis_name = analysis_name
        self.datacollection = db[analysis_name]
        self.datacollection.ensure_index([("date", pymongo.DESCENDING)],
                                          expireAfterSeconds=120*24*3600)  # keep records for 120 days
        self.statuscollection = db[LASTRUNS_COLLECTION_NAME]
        self.statuscollection.ensure_index([("analysis", pymongo.DESCENDING)])
        self._pig_start, self._pig_end = self._get_date_window(datemask);
        self.datemask = self._get_datemask(datemask)
        self.pigscript = pigscriptfile
        self.pigjob_id = None
        self.num_days = self._get_num_days(datemask)
        self.datestrings = self._get_datestrings(datemask)
        log.info("Finished initializing pipeline.")


    def __add_status_entry(self, query, update):
        """Add an entry for this analysis to the status collection."""
        self.statuscollection.update(query, {"$set": update}, upsert=True)


    def _get_last_run_date(self):
        """Return date of most recent pig output that was written to MongoDB."""
        query = {"analysis": self.analysis_name} 
        if not self.statuscollection.find(query):
            today = datetime.datetime.today()
            seed_date = datetime.datetime(year=today.year, month=today.month, day=today.day)
            update = {"last_run_date": seed_date}
            self.__add_status_entry(query, update)
        results = list(self.statuscollection.find(query)) 
        assert len(results) == 1, "Last run status not unique for {}".format(self.analysis_name)
        result = results[0]["last_run_date"]
        return result


    def get_last_pigoutput(self):
        """Return date of most recent raw log that was processed on EMR."""
        query = {"analysis": self.analysis_name}
        results = list(self.statuscollection.find(query))
        assert len(results) <= 1, "Last run status not unique for {}".format(self.analysis_name)
        record = results[0]
        if "latest_pigoutput" in record:
            result = record["latest_pigoutput"]
        else:
            result = None
        return result


    def get_last_available_key(self, path):
        """Return the most recent key in a given S3 path."""
        logfile = None
        for key in EMRMongoPipeline.source_bucket.list(path):
            if "SUCCESS" not in key.name:
                logfile = key.name
        last_log_filename = logfile
        if logfile:
            match = re.search("\d{4}-\d{2}-\d{2}", last_log_filename)
        return datetime.datetime.strptime(match.group(), EMRMongoPipeline.dateformat) if logfile else None


    def _get_date_window(self, datemask):
        """Returns start date, end date of the logs this EMR job will process."""
        log.info("Getting date window")
        if datemask: return [datetime.datetime.strptime(datemask, EMRMongoPipeline.dateformat)] * 2
        else:
            results = list(self.statuscollection.find({"analysis": self.analysis_name}))
            assert len(results) <= 1, "Last run status not unique for {}".format(self.analysis_name)
            # end at the most recent raw input log that has not yet been processed on EMR
            end = self.get_last_available_key(EMRMongoPipeline.processed_logs_path)
            # start from the date after the most recent EMR output
            start = (self.get_last_available_key(self.bucket_prefixes[0]) + datetime.timedelta(1)) if self.get_last_available_key(self.bucket_prefixes[0]) else end 
            return [start, end]


    def _get_datemask(self, datemask):
        """Return regex argument to pass to pig script.
           
           If this analysis is being run for the first time, 
           the user invokes the script with a datemask argument 
           with the format 'YYYY-mm-dd' that specifies
           the date of the first raw log to run an EMR job on.
           After the first time an EMR job runs for this analysis,
           this method can compute the datemask without requiring
           a datemask parameter.

        """
        if datemask: return datemask
        else:
            """ 
            Automatically generate the datemask. If start year 
            and end year are different or start month and end 
            month are different, run EMR jobflow for logs 
            until the end of the current 10-day period.
            
            Examples:
            
            Current date: 11/06
            Last available pig output: 10/24
            => this jobflow will run on logs from 10/25 - 10/29
            (next job will run on logs from 10/30 - 10/31)
            => pig regex = "2013-10-2[56789]"

            Current date: 11/06
            Last available pig output: 11/13
            => this jobflow will run on logs from 11/07 - 11/09
            (next job will run on logs from 11/10 - 11/13)
            => pig regex = "2013-11-0[789]"
            
            """        
            start_year, start_month, start_day = self._pig_start.strftime(EMRMongoPipeline.dateformat).split("-")
            end_year, end_month, end_day = self._pig_end.strftime(EMRMongoPipeline.dateformat).split("-")
            year_mask = start_year
            month_mask = start_month
            return "{year}-{month}-{day}".format(year=year_mask, month=month_mask, day=self._get_mask(start_day, end_day))

    
    def _get_mask(self, start_day, end_day):
        """Helper method for generating the regex argument to pass to the pig script."""
        if (start_day[0] == end_day[0]):
            ones_digits = range(int(start_day[1]), 1 + int(end_day[1]))
        else:
            ones_digits = range(int(start_day[1]), 1 + 9)
        return "{tens}[{ones}]".format(tens=str(start_day[0]), ones="".join(map(str, ones_digits)))


    def _get_num_days(self, datemask):
        """Return number of days of EMR output to process in memory and write to MongoDB."""
        if datemask: num_days = 1
        else:
            if not self._get_last_run_date():
                self.update_lastrun_status()
            try:
                start = self._get_last_run_date() + datetime.timedelta(1)
                end = self.get_last_pigoutput()
                num_days = (end - start).days if end else 1
            except TypeError:
                log.error("""Cannot compute start, end dates when stored value of
                            of last run date is `None`. If this is the first time
                            running the pipeline for an analysis,
                            run with `datemask` argument. Example:\n
                            python [name_of_analysis.py] --datemask 2013-12-01""")
                raise
        return num_days


    def _get_datestrings(self, datemask):
        """Yield search string for finding EMR output in S3 to process and write to MongoDB."""
        if datemask:
            start = datetime.datetime.strptime(datemask, EMRMongoPipeline.dateformat)
        else:
            start = self._get_last_run_date() + datetime.timedelta(1)
        while (self.num_days >= 0):
            yield datetime.datetime.strftime(start, EMRMongoPipeline.dateformat)
            start += datetime.timedelta(1)
            self.num_days -= 1


    def _provision_instances(self):
        """Provisions EC2 instances for the EMR pig jobflow."""
        log.info("Provisioning EC2 instances...")
        return [InstanceGroup(role="MASTER",
                              type="m1.medium",
                              num_instances=1,
                              market="SPOT",
                              name="master",
                              bidprice=str(2.0)),
                InstanceGroup(role="CORE",
                              type="m1.xlarge",
                              num_instances=self._get_num_core_instances(),
                              market="SPOT",
                              name="core",
                              bidprice=str(2.0))]


    def _get_num_core_instances(self):
        """Returns the number of EC2 worker instances to provision for an EMR job.
        
        The number of worker instances to provision increases with the number
        of days of raw log data to process.

        """
        pig_window = (self._pig_end - self._pig_start).days
        if pig_window < 10:
            return 2
        elif (pig_window < 30 and pig_window >= 10):
            return 6
        elif (pig_window >= 30):
            return 12
        else:
            raise ValueError("Invalid value for number of days in date window")


    def run_EMR_jobflow(self):
        """Launches an EMR pig jobflow."""
        jobflow_name = self.pigscript.split(".pig")[0]
        instance_groups = self._provision_instances()
        steps = [InstallPigStep(),
                 PigStep(jobflow_name,
                 self.pigscript,
                 pig_args=['-p', 'baseInputPath={}'.format(EMRMongoPipeline.base_input_path), 
                            '-p', 'datemask={}'.format(self.datemask)])]
        log.info("Establishing connection to EMR")
        emr_connection = boto.emr.connection.EmrConnection()
        self.pigjob_id = emr_connection.run_jobflow(name=jobflow_name,
                                                    steps=steps,
                                                    instance_groups=instance_groups,
                                                    ec2_keyname='emr_jobs',
                                                    log_uri='s3://parselypig/logs/',
                                                    keep_alive=False,
                                                    ami_version='latest',
                                                    enable_debugging=True)
        log.info("EMR jobflow launched")


    def check_jobflow_completion(self, wait_interval=60):
        """Periodically reports the status of a running EMR jobflow."""
        emr_connection = boto.emr.connection.EmrConnection()
        while True:
            jobflow_info = emr_connection.describe_jobflow(self.pigjob_id)
            log.info("jobflow {id} state: {state}".format(id=self.pigjob_id,
                                                          state=jobflow_info.state))
            if jobflow_info.state in ["COMPLETED", "TERMINATED"]:
                return True
            elif jobflow_info.state == "FAILED":
                raise Exception("Jobflow FAILED with message: {}".format(jobflow_info.laststatechangereason))
            # update jobflow state
            time.sleep(wait_interval)


    def move_from_scratch(self):
        """
        Transfer output of the EMR pig jobflow out of scratch directory.
        Move the output of the EMR pig jobflow from the scratch directory it was
        written to to specified directories in S3.

        """
        for prefix in self.bucket_prefixes:
            source_prefix = "temp/{}".format(prefix)
            for s3key in EMRMongoPipeline.source_bucket.list(source_prefix):
                if "success" not in s3key.name.lower():
                    dest_key_name = s3key.name.split("temp/")[1]
                    s3key.copy(EMRMongoPipeline.source_bucket.name, dest_key_name)
                    log.info("copied {}".format(dest_key_name))
        for old_key in EMRMongoPipeline.source_bucket.list("temp"):
            old_key.delete()
            log.info("deleted {}".format(old_key.name))


    def write_EMR_output(self, mem_efficient=False):
        """Processes pig output in memory and writes records to MongoDB."""
        for datestring in self.datestrings:
            log.info("Started to process EMR output for {}".format(datestring))
            date = datetime.datetime.strptime(datestring, EMRMongoPipeline.dateformat)
            for results in self._parse_data(datestring, mem_efficient):
                records = self._get_final_records(results, date)
                self._write_as_json(records)
                write_json_to_db = subprocess.Popen(["mongoimport", 
                                                        "--db",
                                                        MONGODB_NAME,
                                                        "--collection",
                                                        self.analysis_name,
                                                        EMRMongoPipeline.json_out],
                                                         stderr=subprocess.STDOUT)
                if check_subprocess("write_json_to_db", write_json_to_db.pid, datestring):
                    remove_imported = subprocess.Popen(["rm", EMRMongoPipeline.json_out], stderr=subprocess.STDOUT)
                    check_subprocess("remove_imported", remove_imported.pid, datestring)
 

    @abstractmethod
    def _parse_data(self, datestring, mem_efficient):
        """Return a list of one or more dictionaries containing MongoDB records.
        
        If mem_efficient is not set, all records will be contained in a single
        dictionary. If mem_efficient is set, records are divided into multiple
        dictionaries.
        
        Implement according to the output of the executed pig jobflow.
        
        """
        data = defaultdict(str)
        yield data


    @abstractmethod
    def _get_final_records(self, data, date):
        """Format dict entries into MongoDB records and serve as iterator."""
        for k, v in data:
            yield {k: v}


    @abstractmethod
    def _filter_records(self, data, n):
        """Filter out records in the long tail to limit storage.

        n can be any threshold used to filter the data:
        minimum number of pageviews for a record to be 
        have enough mass to be stored in the database,
        maximum number of records to store per day, etc.  

        """
        return data


    def _get_raw_output(self, bucket_prefix):
        """Uncompress EMR output files and iterate through lines in the output logs."""
        keys = EMRMongoPipeline.source_bucket.list(bucket_prefix)
        for s3key in keys:
            s3key.get_contents_to_filename(EMRMongoPipeline.writebuffer)
            proc = subprocess.Popen(['zcat', EMRMongoPipeline.writebuffer],
                                    stdout=subprocess.PIPE)
            data = StringIO(proc.communicate()[0])
            for record in data:
                yield record


    def _write_as_json(self, records):
        """Serialize python dict items into a json file of database records."""
        with open(EMRMongoPipeline.json_out, "a+b") as f:
            for record in records:
                f.write("{}\n".format(json_util.dumps(record)))
        log.info("Finished writing records to {}".format(EMRMongoPipeline.json_out))


    def update_lastrun_status(self):
        """Update date of most recent pig output that has been processed and stored locally."""
        dates = self.datacollection.distinct("date")
        new_status = max(dates) if dates else None
        db.last_run_status.update({"analysis": self.analysis_name}, {"$set": {"last_run_date": new_status}}, upsert=True)


    def update_latest_pigoutput(self):
        """Update the date of the most recent pig output that has been written to S3."""
        latest_pig_complete = self.get_last_available_key(self.bucket_prefixes[0])
        db.last_run_status.update({"analysis": self.analysis_name},
                                    {"$set":{"latest_pigoutput":latest_pig_complete}}, upsert=True)
