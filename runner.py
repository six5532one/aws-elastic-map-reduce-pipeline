"""
Performs a series of one or more data analysis pipelines.

Instantiates an object to encapsulate each data analysis pipeline 
and iterates through the list of desired analyses, performing each 
in the following steps:
    1. Deduce the dates for which there exists outstanding raw logs to process with EMR
    2. Run an EMR job on raw logs corresponding to those dates
    3. Move output of EMR job from scratch directory to permanent S3 path
    4. Process EMR output in memory as needed before storing to local MongoDB instance

Keeps records of the most recent EMR output stored permanently in S3 and the
most recent data written to MongoDB locally. For storage info, see settings.py.

"""

import logging

from raven import Client
from pipelines import *
from settings import SENTRY_KEY

def run(pipeline, emr=True):
    log = logging.getLogger()
    sentry_client = Client(dsn=SENTRY_KEY)
 
    # record date of last log processed via python
    pipeline.update_lastrun_status()
    
    # Run EMR job if user did not pass the flag to skip this step.
    if emr:
        if pipeline.get_last_pigoutput() and (analysis.get_last_available_key(EMRMongoPipeline.processed_logs_path) == analysis.get_last_pigoutput()):
            log.info("No outstanding EMR input to process.")
        else:
            pipeline.run_EMR_jobflow()
            try:
                completed = pipeline.check_jobflow_completion()
                if completed:
                    pipeline.move_from_scratch()
            except Exception as e:
                log.error(e.message)
            finally:
                # record date of last log processed via EMR
                pipeline.update_latest_pigoutput()

    # Shape EMR output logs into MongoDB records.
    try:
        pipeline.write_EMR_output()
    except OSError as e:
        log.error("{}: retrying write_EMR_output() with mem_efficient option".format(e.message))
        try:
            pipeline.write_EMR_output(mem_efficient=True)
        except OSError as e:
            log.error("{}: mem efficient write_EMR_output()() failed. Wait for file descriptors to be garbage collected....................................................................................................................................................................................................................................................".format(e.message))
            time.sleep(GARBAGE_COLLECTION_PERIOD * 60)
            try:
                pipeline.write_EMR_output(mem_efficient=True)
            except OSError as e:
                log.error("write_EMR_output() failed for {} after waiting for file descriptors to be garbage collected.".format(pipeline.collection_name))
